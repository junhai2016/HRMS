<?php
/**
 * Created by PhpStorm.
 * User: Shen
 * Date: 2015/4/7
 * Time: 20:24
 */
namespace Home\Model;
use Think\Model\RelationModel;
class PositionModel extends  RelationModel{

    protected $_validate = array(     
            array('name','require','职位名称必须！'),
        );

//    获取职位
    public function getPositions($number,$order){
        $count = $this->count();
        $Page  = new \Library\Page($count,$number);// $number为分页数
        $Page->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% <li>%HEADER%</li>');
        $data['page'] = $Page->show();// 分页显示输出
        $data['position'] =
            $this->order($order)
                ->limit($Page->firstRow.','.$Page->listRows)
                ->select();
        return $data;
    }
}