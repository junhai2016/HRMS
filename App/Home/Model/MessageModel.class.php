<?php
/**
 * Created by PhpStorm.
 * User: Shen
 * Date: 2015/4/1
 * Time: 20:32
 */
namespace Home\Model;
use Think\Model\RelationModel;
class MessageModel extends RelationModel{

    protected $_link = array(
        'user' => array(
            'mapping_type' => self::BELONGS_TO,
            'class_name'   => 'user',
            'foreign_key'  => 'user_id',
            'mapping_fields'=>'username',
            'as_fields'   =>  'username:user'
        )


    );
//    获取通告
    public function getMessages($number,$where,$order){

        $count = $this->where($where)->count();
        $Page = new \Library\Page($count,$number);
        $Page ->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% <li>%HEADER%</li>');
        $data['page'] = $Page ->show();
        $data['message'] = $this ->where($where)
                                 ->order($order)
                                 ->limit($Page->firstRow.','.$Page->listRows)
                                 ->relation(true)
                                 ->select();
        return $data;

    }

}