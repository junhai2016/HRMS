<?php
return array(
	//'配置项'=>'配置值'
	//资源路径
	'TMPL_PARSE_STRING' => array(
	'__STATIC__' 		=> __ROOT__. '/App/' . MODULE_NAME . '/View/Public',
    '__IMG__'    		=> __ROOT__. '/App/' . MODULE_NAME . '/View'.'/Public/img',
    '__CSS__'    		=> __ROOT__. '/App/' . MODULE_NAME . '/View'.'/Public/css',
    '__JS__'     		=> __ROOT__. '/App/' . MODULE_NAME . '/View'.'/Public/js',
    '__BOOTSTRAP__'		=> __ROOT__. '/App/' . MODULE_NAME . '/View'.'/Public/bootstrap',
    '__EDITOR__'		=> __ROOT__. '/Public/kindeditor'
	)
);