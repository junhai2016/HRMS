<?php
/**
 * Created by PhpStorm.
 * User: Shen
 * Date: 2015/3/24
 * Time: 20:45
 */
namespace Home\Controller;
use Think\Controller;
class MessageController extends CommonController{


    public function index(){
        $data = D("Message")->getMessages(8,array("trash"=>0),"id asc");
//        var_dump($data);die;
        $this->message = $data['message']; //文章列表
        $this->page    = $data['page'];   //分页导航
        $this->assign("title","通告列表");
        // var_dump($data);die;
        $this->display();
    }


// 显示添加通告界面
    public function add(){
    	$this->display();
    }

  // 添加通告操作
  	public function addHandle(){
  		if(!IS_POST) $this->error("请求异常！");
  		// var_dump(I());die;
  		$message= M("message");
      if(I('title') == '')$this->error('标题不能为空');
  		$data['title'] = I('title');
  		$data['content'] = I('content1');
  		$data['user_id'] = session('uid');
  		$data['release_time'] = time();
  		// var_dump($data);die;
  		if (!$message->create($data)) {
  			$this->error($message->getError());
  		}else{
  			if($message->add($data)){
  				$this->success("发表成功！",__MODULE__."/Message/");
  			}else{
  				$this->error("发表失败！");
  			}
  		}

  	}

    // 编辑通告
    public function edit(){
        $message = M('message');
        $data = $message->where('id=%d',I('id'))->find();
        // var_dump($data);die;
        $this->id = $data['id'];
        $this->title = $data['title'];
        $this->content1 = $data['content'];
        $this->display();
    } 

    // 编辑操作
    public function editHandle(){
        if(!IS_POST) $this->error("请求异常！");
      
        $message = M('message');
        $data = $message->where('id=%d',I('id'))->find();
        $data['title'] = I('title') ;
        $data['content'] = I('content1');
        // var_dump($data);die;
        if($message->save($data)){
           $this->success("更新成功！",__MODULE__."/Message/");
         }else{
          $this->error("更新失败！");
        }

    }

    // 通告单页
    public function cate(){
      // var_dump(I());die;
      $message = M("message")->where('id=%d',I('id'))->find();
      // var_dump($message);die;
      $user = M('user')->where('id=%d',$message['user_id'])->find();

      $this->user = $user['username'];
      $this->title = $message['title'];
      $this->content = $message['content'];
      $this->date = $message['release_time'];
      $this->display();
    }

  	// 删除通告
  	public function del(){
  		$message = M('message');
    	if ($message->delete(I('id'))) {
    		$this->success('删除成功',__MODULE__."/Message/");
    	}else{
    		$this->error('删除失败');
    	}
  	}



}