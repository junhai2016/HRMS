<?php
/**
 * Created by PhpStorm.
 * User: Shen
 * Date: 2015/3/24
 * Time: 20:45
 */
namespace Home\Controller;
use Think\Controller;
class SystemController extends CommonController{


    public function index(){
        $data['serverSoft'] = $_SERVER['SERVER_SOFTWARE'];
        $data['serverOs'] = PHP_OS;
        $data['phpVersion'] = PHP_VERSION;
        $data['fileupload'] = ini_get('file_uploads') ? ini_get('upload_max_filesize') : '禁止上传';
        $data['serverUri'] = $_SERVER['SERVER_NAME'];
        $data['maxExcuteTime'] = ini_get('max_execution_time') . ' 秒';
        $data['maxExcuteMemory'] = ini_get('memory_limit');
        //获取数据库大小
        $dbsize= 0;
        $dblength = M()->query('SHOW TABLE STATUS LIKE\''.C('DB_PREFIX').'%\'');
        foreach ($dblength as $v) {
            $dbsize += $v['Data_length'] + $v['Index_length'];
        }
        $data['dbsize'] = $dbsize/1024; //这里是Kb
        //获取mysql版本
        $data['mysql_version'] = M()->query('SELECT version() AS version');
        $this->assign('data',$data);
//        var_dump($data);die;
        $this->display();
    }

    public function welcome(){
        $this->display();
    }

}