<?php
return array(
	//'配置项'=>'配置值'

//    'MODULE_ALLOW_LIST'    =>   array('Home'),  //允许访问的模块列表
    // 'MULTI_MODULE'         =>   false,  //开启单模块模式
    'DEFAULT_MODULE'       =>   'Home', //设置默认模块
    'SHOW_PAGE_TRACE'      =>   true,   //开启调试工具
    'LOAD_EXT_CONFIG'      =>   'db,auth',   //加载扩展配置
    'URL_CASE_INSENSITIVE' =>   true,   //URL不区分大小写
    'URL_MODEL'            =>   3,      //URL模式
    'URL_HTML_SUFFIX'	   =>   'shtml' //伪静态

);